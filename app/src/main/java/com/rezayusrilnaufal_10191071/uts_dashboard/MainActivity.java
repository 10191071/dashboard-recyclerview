package com.rezayusrilnaufal_10191071.uts_dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;

    GridLayoutManager gridLayoutManager;

    DashBoardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rlmenu);

        addData();
        gridLayoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashBoardAdapter(datamenu);
        recyclerView.setAdapter(adapter);
    }

    public void addData(){
        datamenu = new ArrayList<>();
        datamenu.add(new SetterGetter("Komputer & Aksesoris", "logomenu1"));
        datamenu.add(new SetterGetter("Fashion", "logomenu2"));
        datamenu.add(new SetterGetter("FUrniture", "logomenu3"));
        datamenu.add(new SetterGetter("Otomotif", "logomenu4"));
        datamenu.add(new SetterGetter("Olahraga", "logomenu5"));
        datamenu.add(new SetterGetter("Kosmetik", "logomenu6"));
        datamenu.add(new SetterGetter("Gadget", "logomenu7"));
        datamenu.add(new SetterGetter("Alat Tulis", "logomenu8"));
        datamenu.add(new SetterGetter("Voucher", "logomenu9"));
    }
}